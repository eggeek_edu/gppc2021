CC       = g++
CFLAGS   = -W -Wall -O3
DEVFLAGS = -W -Wall -ggdb -O0
EXEC     = run

all:
	$(CC) $(CFLAGS) -o $(EXEC) *.cpp
dev:
	$(CC) $(DEVFLAGS) -o $(EXEC) *.cpp
